const CryptoNFT = artifacts.require('./CryptoNFT.sol');

contract('CryptoNFT', (accounts) => {
  let contract;

  before(async () => {
    contract = await CryptoNFT.deployed();
  });

  it('...get deployed', async () => {
    assert.notEqual(contract, '');
  });

  it('... gets minted and added', async () => {
    const result = await contract.mint('first_test');

    let coder = await contract.coders(0);

    console.log(coder);
    assert(coder, 'first_test');
  });
});
